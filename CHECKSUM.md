# Hummingbird for Linux and macOS

#### AirVPN's free and open source OpenVPN 3 client based on AirVPN's OpenVPN 3 library fork

### Version 1.1.2 - Release date 4 June 2021


## Note on Checksum Files

Hummingbird is an open source project and, as such, its source code can be
downloaded, forked and modified by anyone who wants to create a derivative
project or build it on his or her computer. This also means the source code can
be tampered or modified in a malicious way, therefore creating a binary version
of hummingbird which may act harmfully, destroy or steal your data, redirecting
your network traffic and data while pretending to be the "real" hummingbird
client genuinely developed and supported by AirVPN.

For this reason, we cannot guarantee forked, modified and custom compiled
versions of Hummingbird to be compliant to our specifications, development and
coding guidelines and style, including our security standards. These projects,
of course, may also be better and more efficient than our release, however we
cannot guarantee or provide help for the job of others.

You are therefore strongly advised to check and verify the checksum codes found
in the `.sha512` files to exactly correspond to the ones below, that is, the
checksum we have computed from the sources and distribution files directly
compiled and built by AirVPN. This will make you sure about the origin and
authenticity of the hummingbird client. Please note the files contained in the
distribution tarballs are created from the very source code available in the
master branch of the [official hummingbird's repository](https://gitlab.com/AirVPN/hummingbird).


### Checksum codes for Version 1.1.2

The checksum codes contained in files `hummingbird-<os>-<arch>-1.1.2.<archive>.sha512`
and `hummingbird.sha512` must correspond to the codes below in order to prove
they are genuinely created and distributed by AirVPN.

## Linux i686

`hummingbird-linux-i686-1.1.2.tar.gz`:
9b4062617da6314169de0d992048e258ce08d16db7403a2625a4ac3cb14be10e16ace266715ca7e9f4e0e94e8ca4976501af8fdb17b4f188c1d9f766268691ce

`hummingbird`:
3ea3277b74b4c83e044b6df6a7e2ef0c3442ad4af89c9fcd9966c1f61d8a0bb996ba11991ad77150f415dd6cb76892eb83f6a2556550cd76391485011523651f


## Linux x86_64

`hummingbird-linux-x86_64-1.1.2.tar.gz`:
88392814ac7b3e5ecb4037817c73e514bb6e62588f2fe1d69a2a813b1793aaff0a8baeb4cb16d177920235882ef4831ade8f5390d981f1f1c03a5cc3d9205f67

`hummingbird`:
38267de94127ac51944fff56ecad087ac5af13d60baf93e02ebe5ce5b70afb2e252519a4f8f5322e21a27f75f9be9ea41d7cdbdb9c1783bae281d4290a296c8b


## Linux ARM32

`hummingbird-linux-armv7l-1.1.2.tar.gz`:
519d14aa086e01fc6df7bb6ffadbd6cfe30d1b8f96b00985f8db1192953a05a9b7b39e01def35de90ee8f01c7f60f08ff8f73d5c1ee2a1c6b05ceb938d313759

`hummingbird`:
3c7152c7e6096c92321f8499af37b85da7b5ab87033f3029115447f9465c322901f1a2a76e0dbae78cab4e7617370fbc4b078c22940fffe8fcf30a327611839f


## Linux ARM64

`hummingbird-linux-aarch64-1.1.2.tar.gz`:
593a19cc250463dfb05b46fbb20a0549f4ed7be4bef6798b62a7599a2f0d13c3ce52162c7f5f0eaed810c293e1a21c721fd9b679b96afe6a2d4272764fd6b6fe

`hummingbird`:
d1b1ef45cc6bbd85ae48265627199d03b20add89c4d6e8c774f60c066122c48bc9156a520e69c327959faa22eefc1b56584091ea352bf8e9bdf4ba7058f70882


## macOS ARM64 plain

`hummingbird-macos-arm64-1.1.2.tar.gz`:
2ef1ec115f21f559797ec8106f2ed7f1af2158dc5fbcb21d961fdcc9458823607dee2543b0fc4ae6db53a11a0d02daa76375d54af059900e14fb6a4472d8edd4

`hummingbird`:
15c621b3876d2eeace09616baa574e396b62fdd8d67762a47630c614349bb145f1573ef489be93033a216c6b1d61c00a693754c6eb5251fd95736e8a61be9fb9


## macOS ARM64 notarized

`hummingbird-macos-arm64-notarized-1.1.2.zip`:
d686e5408aeda98a334758ba495c03280319c70904209b97fc05b3dc93c5ae1086096fea25bb5c44059a41e239ba560f80b92ceba16b0439e60239614a3477cb


## macOS x86_64 plain

`hummingbird-macos-x86_64-1.1.2.tar.gz`:
fc84cf10e28f5596ce94be612fda09f79c7793045f227595a50622f58904efdb0907f1113fb4670e87c4596247705289541ca2a207370c039d7a456296629152

`hummingbird`:
dbc9e869837e730b5edb6d75614397b3b9fbb0e93a863520bac10f94c16e2a183a0dbe536bd6dccbf19b657221da031629c2d21ff836b21da9d5ccc911605e3d


## macOS x86_64 notarized

`hummingbird-macos-x86_64-notarized-1.1.2.zip`:
a5c5d8ab533ee2244e23ea538e1f3d806aea367644883947278aaa738b0c169c45c99680b490a7b8740d2e698e69c621880babfcb6bcdbf1d1fedbe4cab3aee8
