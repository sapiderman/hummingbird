/*
 * optionparser.cpp
 *
 * This file is part of AirVPN's Linux/macOS OpenVPN Client software.
 * Copyright (C) 2019-2021 AirVPN (support@airvpn.org) / https://airvpn.org
 *
 * Developed by ProMIND
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Hummingbird. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "include/optionparser.hpp"
#include "include/airvpntools.hpp"

#include <fstream>
#include <algorithm>

extern void global_syslog(std::string s);

OptionParser::OptionParser()
{
    setup();
}

OptionParser::~OptionParser()
{
    int i;

    for(i = 0; i < option.size(); i++)
        delete option[i];

    for(i = 0; i < configOption.size(); i++)
        delete configOption[i];
}

void OptionParser::setup()
{
    errorDescription = "";

    configOption.clear();

    option.clear();
}

bool OptionParser::addConfigOption(std::string sname, std::string lname, Type type)
{
    OptionConfig *config;

    if(sname.empty() && lname.empty())
        return false;

    if(sname.empty())
        sname = lname;

    if(lname.empty())
        lname = sname;

    config = new OptionConfig;

    if(config == nullptr)
        return false;

    config->shortName = sname;
    config->longName = lname;
    config->type = type;

    configOption.push_back(config);

    return true;
}

OptionParser::OptionConfig OptionParser::getConfigOption(std::string name)
{
    OptionConfig config;
    int pos = 0;

    config.shortName = "";
    config.longName = "";
    config.type = Type::UNDEFINED;
    
    if(name.empty())
        return config;

    if(name.substr(0, 2) == "--")
        pos = 2;
    else if(name[0] == '-')
        pos = 1;
    else
        pos = 0;

    if(pos > 0)
        name = name.substr(pos);

    for(OptionConfig *opt : configOption)
    {
        if(opt->shortName == name || opt->longName == name)
        {
            config.shortName = opt->shortName;
            config.longName = opt->longName;
            config.type = opt->type;
            
            break;
        }
    }
    
    return config;
}

bool OptionParser::isOptionEnabled(Option *opt)
{
    bool isEnabled = false;

    if(opt == nullptr)
        return false;

    if(opt->type != Type::BOOL)
        return false;

    if(isBoolEnabled(opt->value) == true)
        isEnabled = true;

    return isEnabled;
}

OptionParser::Error OptionParser::parseOptions(std::vector<std::string> opt)
{
    bool done = false;
    int ndx = 0;
    Option *newOption;
    OptionConfig currentOption;
    std::string currentItem, nextItem;

    errorDescription = "";

    if(configOption.empty())
    {
        errorDescription = "Configuration options have not been set up";

        return Error::NO_OPTIONS_PROVIDED;
    }

    if(opt.empty())
    {
        errorDescription = "Option vector is empty";

        return Error::NO_OPTIONS_PROVIDED;
    }

    option.clear();
    
    while(done == false)
    {
        currentItem = opt[ndx];

        if(ndx < opt.size() - 1)
            nextItem = opt[ndx + 1];
        else
            nextItem = "";

        if(currentItem[0] != '-')
        {
            errorDescription = "\"";
            errorDescription += currentItem;
            errorDescription += "\" is not a valid argument";

            return Error::SYNTAX_ERROR;
        }

        currentOption = getConfigOption(currentItem);

        if(currentOption.type == UNDEFINED)
        {
            errorDescription = "Unknown option " + currentItem;

            return Error::UNKNOWN_OPTION;
        }

        newOption = new Option;

        if(newOption == nullptr)
        {
            errorDescription = "Cannot allocate memory for options";

            return Error::PARSE_ERROR;
        }

        newOption->shortName = currentOption.shortName;
        newOption->longName = currentOption.longName;
        newOption->type = currentOption.type;

        switch(currentOption.type)
        {
            case OPTION:
            {
                if(nextItem[0] == '-' || nextItem == "")
                {
                    newOption->isValid = true;
                    newOption->value = "";
                    newOption->error = "";
                
                    ndx++;
                }
                else
                {
                    newOption->isValid = false;
                    newOption->value = "";
                    newOption->error = "Argument is not required";
                    
                    ndx += 2;
                }
            }
            break;

            case INTEGER:
            {
                if(nextItem[0] != '-' && nextItem != "")
                {
                    newOption->value = nextItem;

                    if(nextItem.find_first_not_of("+-0123456789") != std::string::npos)
                    {
                        newOption->isValid = false;
                        newOption->error = "Argument is not an integer number";
                    }
                    else
                    {
                        newOption->isValid = true;
                        newOption->error = "";
                    }

                    ndx += 2;
                }
                else
                {
                    newOption->isValid = false;
                    newOption->value = "";
                    newOption->error = "Option requires an integer argument";
                    
                    ndx++;
                }
            }
            break;

            case NUMBER:
            {
                if(nextItem[0] != '-' && nextItem != "")
                {
                    newOption->value = nextItem;

                    if(nextItem.find_first_not_of("+-.0123456789") != std::string::npos)
                    {
                        newOption->isValid = false;
                        newOption->error = "Argument is not a number";
                    }
                    else
                    {
                        newOption->isValid = true;
                        newOption->error = "";
                    }

                    ndx += 2;
                }
                else
                {
                    newOption->isValid = false;
                    newOption->value = "";
                    newOption->error = "Option requires a number argument";
                    
                    ndx++;
                }
            }
            break;

            case STRING:
            {
                if(nextItem[0] != '-' && nextItem != "")
                {
                    newOption->isValid = true;
                    newOption->value = nextItem;
                    newOption->error = "";
                
                    ndx += 2;
                }
                else
                {
                    newOption->isValid = false;
                    newOption->value = "";
                    newOption->error = "Option requires a string argument";
                    
                    ndx++;
                }
            }
            break;

            case BOOL:
            {
                if(nextItem[0] != '-' && nextItem != "")
                {
                    nextItem = AirVPNTools::toLower(nextItem);

                    newOption->value = nextItem;

                    if(isValidBool(nextItem) == true)
                    {
                        newOption->isValid = true;
                        newOption->error = "";
                    }
                    else
                    {
                        newOption->isValid = false;
                        newOption->error = allowedBoolValueMessage("Argument");
                    }
                
                    ndx += 2;
                }
                else
                {
                    newOption->isValid = false;
                    newOption->value = "";
                    newOption->error = "Option requires a boolean argument";
                    
                    ndx++;
                }
            }
            break;
            
            case UNDEFINED:
            {
                newOption->shortName = currentItem;
                newOption->longName = currentItem;
                newOption->isValid = false;
                newOption->value = "";
                newOption->error = "Unknown option";

                ndx++;
            }
            break;
        }

        option.push_back(newOption);

        if(ndx >= opt.size())
            done = true;
    }

    return Error::OK;
}

OptionParser::Options OptionParser::getOptions()
{
    return optionVector(true);
}

OptionParser::Options OptionParser::getInvalidOptions()
{
    return optionVector(false);
}

OptionParser::Option *OptionParser::getOption(std::string name)
{
    Option *opt = nullptr;

    for(int i = 0; i < option.size() && opt == nullptr; i++)
    {
        if((option[i]->shortName == name || option[i]->longName == name) && option[i]->isValid == true)
            opt = option[i];
    }

    return opt;
}

OptionParser::Options OptionParser::optionVector(bool valid)
{
    Options item;

    item.clear();

    for(int i = 0; i < option.size(); i++)
    {
        if(option[i]->isValid == valid)
            item.push_back(option[i]);
    }

    return item;
}

std::string OptionParser::getErrorDescription()
{
    return errorDescription;
}

bool OptionParser::isValidBool(std::string value)
{
    bool isValid = false;
    
    if(value == "yes" || value == "no" || value == "on" || value == "off" || value == "true" || value == "false" || value == "1" || value == "0")
        isValid = true;

    return isValid;
}

bool OptionParser::isBoolEnabled(std::string value)
{
    bool isEnabled = false;

    if(value == "yes" || value == "on" || value == "true" || value == "1")
        isEnabled = true;
    
    return isEnabled;
}

std::string OptionParser::allowedBoolValueMessage(std::string option)
{
    return option + " can be yes, on, true or 1 to enable it; no, off, false or 0 to disable it";
}

